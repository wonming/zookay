<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->

    <!-- Head BEGIN -->
    <head>
        <meta charset="utf-8">
        <title>Zookay One Page</title>

        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <link rel="shortcut icon" href="favicon.ico">
        <!-- Fonts START -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Pathway+Gothic+One|PT+Sans+Narrow:400+700|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
        <!-- Fonts END -->
        <!-- Global styles BEGIN -->


        <link href="{{ URL::to('/') }}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
        <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="../../assets/global/plugins/slider-revolution-slider/rs-plugin/css/settings.css" rel="stylesheet">
        <!-- Global styles END -->
        <!-- Page level plugin styles BEGIN -->
        <link href="../../assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
        <!-- Page level plugin styles END -->
        <!-- Theme styles BEGIN -->
        <link href="../../assets/global/css/components.css" rel="stylesheet">
        <link href="../../assets/frontend/onepage/css/style.css" rel="stylesheet">
        <link href="../../assets/frontend/onepage/css/style-responsive.css" rel="stylesheet">
        <link href="../../assets/frontend/onepage/css/themes/red.css" rel="stylesheet" id="style-color">
        <link href="../../assets/frontend/onepage/css/custom.css" rel="stylesheet">
        <!-- Theme styles END -->
    </head>
    <!--DOC: menu-always-on-top class to the body element to set menu on top -->
    <body class="menu-always-on-top">
        <!-- BEGIN STYLE CUSTOMIZER -->
        <!-- END BEGIN STYLE CUSTOMIZER -->
        <!-- Header BEGIN -->
        <div class="header header-mobi-ext" style="background: none; box-shadow: none;">
            <div class="container">
                <div class="row">
                    <!-- Logo BEGIN -->
                    <div class="col-md-2 col-sm-2">
                      <!-- <a class="scroll site-logo" href="#promo-block"><img src="../../assets/frontend/onepage/img/logo/red.png" alt="Metronic One Page"></a> -->
                    </div>
                    <!-- Logo END -->
                    <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>
                    <!-- Navigation BEGIN -->
                    <div class="col-md-5 pull-right">
                        <ul class="header-navigation">
                            <!-- <li class="current"><a href="#promo-block">Home</a></li> -->
                            <li><button class="btn btn-danger btn-circle" href="#">Zookay Installieren</button></li>
                            <li><a href="#">$8</a></li>
                            <li><a href="#">News</a></li>
                            <li><a href="#"><i class="fa fa-bars"></i></a></li>
                        </ul>
                    </div>
                    <!-- Navigation END -->
                </div>
            </div>
        </div>
        <!-- Header END -->

        <div class="promo-block" id="page1">
            <div class="tp-banner-container">
                <div class="tp-banner" >
                    <ul>
                        <li data-transition="fade" data-slotamount="5" data-masterspeed="700" data-delay="9400" class="slider-item-1">
                            <img src="../../assets/frontend/onepage/img/back1.jpg" alt="" data-bgfit="cover" style="opacity:0.4 !important;" data-bgposition="center center" data-bgrepeat="no-repeat">
                            <div class="tp-caption large_text customin customout start"
                                 data-x="center"
                                 data-hoffset="0"
                                 data-y="center"
                                 data-voffset="60"
                                 data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="1000"
                                 data-start="500"
                                 data-easing="Back.easeInOut"
                                 data-endspeed="300">
                                <div class="form-group col-md-5 col-xs-10">
                                    <input type="text" class="form-control"/>
                                    <i class="fa fa-search"></i>
                                </div>
                                <!--<br>&nbsp;-->
                                <div class="comment-currency text-center"><font style="color:red;">$</font>38.941.987</div>
                                <div class="comment text-center">Baume wurden von Ecosia-Nutzern gepflanzt</div>
                                <div class="comment text-center"><a>MEHR ERFAHREN</a></div>
                            </div>
                            <div class="tp-caption large_bold_white fade"
                                 data-x="center"
                                 data-y="center"
                                 data-voffset="-110"
                                 data-speed="300"
                                 data-start="1700"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power1.easeIn"
                                 data-captionhidden="off"
                                 style="z-index: 6">
                                 <!-- Parallax <span>One Page</span> Has Arrived -->
                                <img src="assets/frontend/onepage/img/logo.png">
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <a href="#page2" class="scroll"><i class="fa fa-arrow-circle-down pagedown-arrow"></i></a>
        </div>

        <div class="promo-block" id="page2">
            <div class="tp-banner-container">
                <div class="tp-banner" >
                    <ul>
                        <li data-transition="fade" data-slotamount="5" data-masterspeed="700" data-delay="9400" class="slider-item-1">
                            <img src="../../assets/frontend/onepage/img/back2.jpg" alt="" data-bgfit="cover" style="opacity:0.4 !important;" data-bgposition="center center" data-bgrepeat="no-repeat">
                            <div class="tp-caption large_bold_white fade"
                                 data-x="center"
                                 data-y="center"
                                 data-voffset="-110"
                                 data-speed="300"
                                 data-start="1700"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power1.easeIn"
                                 data-captionhidden="off"
                                 style="z-index: 6">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-5 col-xs-10">
                                            <p class="title margin-bottom-50">Uberschrift</p>
                                            <p class="margin-top-20" style="font-size: 19px; line-height: 30px;">
                                                Lorem ipsum dolor amet, tempor ut labore magna tempor dolore
                                                Lorem ipsum dolor amet, tempor ut labore magna tempor dolore
                                                Lorem ipsum dolor amet, tempor ut labore magna tempor dolore
                                            </p>
                                            <button class="btn blue margin-top-20 btn-circle">installerien for chrome</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <a href="#page3" class="scroll"><i class="fa fa-arrow-circle-down pagedown-arrow"></i></a>
        </div>

        <div class="team-block content content-center" id="page3">
            <div class="container">
                <h2 class="margin-top-20" style="text-transform: none;">Fur ein besseres Internet</h2>
                <div class="row">
                    <div class="col-md-4 col-xs-6 item">
                        <img src="../../assets/frontend/onepage/img/ele1.png" alt="" class="img-responsive">
                        <h3 class="margin-bottom-20">Komplett transparent</h3>
                        <p>
                            Wir veroffentlichen monatliche Finanzberichte, 
                            sodass du immer nachvollziehen kannst,
                            wohin die Einnahmen aus deinen Suchanfragen flienBen.
                        </p>
                    </div>
                    <div class="col-md-4 col-xs-6 item">
                        <img src="../../assets/frontend/onepage/img/ele2.png" alt="" class="img-responsive">
                        <h3 class="margin-bottom-20">mehr als CO<sub>2</sub>-neutral</h3>
                        <p>
                            Unsere Server werden zu 100% aus erneuerbaren Energien
                            betrieben, und jeder gepflanzte
                            Baum entzieht der Atmosphare
                            1kg CO<sub>2</sub>
                        </p>
                    </div>
                    <div class="col-md-4 col-xs-6 item">
                        <img src="../../assets/frontend/onepage/img/ele3.png" alt="" class="img-responsive">
                        <h3 class="margin-bottom-20">Datenschutzf renundlich</h3>
                        <p>
                            Wir verkaufen keine Daten an
                            Werbende, verwenden keine
                            Drittanbieter-Tracker und
                            anonymisieren samtliche
                            Suchdaten innerhalb einer Woche.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <button class="btn blue margin-bottom-50 btn-circle">installerien for chrome</button>
                </div>
            </div>
            <a href="#page4" class="scroll"><i class="fa fa-arrow-circle-down pagedown-arrow"></i></a>
        </div>

        <div class="portfolio-block content content-center" id="page4">
            <div class="row" style="background-color: rgb(113,202,244);">
                <div class="item col-md-6 col-sm-12 col-xs-12">
                    <img src="../../assets/frontend/onepage/img/elephant.png" alt="NAME" class="img-responsive">
                </div>
                <div class="item col-md-6 col-sm-12 col-xs-12">
                    <div class="pull-left">
                        <img src="../../assets/frontend/onepage/img/wwf.png" alt="NAME" class="img-responsive media-object">
                    </div>
                    <div class="row">
                        <div class="col-md-12 valign-center">
                            <h1 class="elephant-title margin-bottom-50">Uberschrift</h1>
                            <div class="row text-justify elephant-text margin-bottom-50">
                                <!--<div class="col-md-offset-1"></div>-->
                                <div class="col-md-6 col-xs-6">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.
                                </div>
                            </div>
                            <div class="row margin-bottom-100" style="text-align: center;">
                                <button class="btn blue btn-circle">INSTALLIEREN FUR CHROME</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#page5" class="scroll"><i class="fa fa-arrow-circle-down pagedown-arrow"></i></a>
        </div>

        <div class="team-block content content-center" id="page5" style="padding-top: 60px !important;">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12 item">
                        <img src="../../assets/frontend/onepage/img/bear.png" alt="" class="img-responsive">
                        <h3>Wir pflanzen Baume dort, wo sie am dringendsten gebraucht werden</h3>
                        <p>Unsere Baume sind gut fur Mensch, Umwelt und die lokale Wirschaft.</p>
                    </div>
                    <div class="col-md-6 col-xs-12 item">
                        <h3>Wir pflanzen Baume dort, wo sie am dringendsten gebraucht werden</h3>
                        <p>Ecosai ist nicht wie jede andere Suchmaschire denn es gibt einen entscheidenden Unterschied: Wir verwenden unsere Gewinne, um Baume zupflanzen.</p>
                        <button class="btn blue margin-top-20 margin-bottom-20 btn-circle">INSTALLIEREN FUR CHROME</button>
                        <img src="../../assets/frontend/onepage/img/dolphin.png" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
            <a href="#page6" class="scroll"><i class="fa fa-arrow-circle-down pagedown-arrow"></i></a>
        </div>

        <div class="portfolio-block content content-center" id="page6">
            <div class="row" style="background-color: rgb(18,79,134);">
                <div class="item col-md-6 col-sm-6 col-xs-12">
                    <h1 class="elephant-title padding-top-70 margin-bottom-50">Comming Soon</h1>
                    <div class="row text-justify elephant-text margin-bottom-50">
                        <div class="col-md-2 col-xs-2"></div>
                        <div class="col-md-8 col-xs-8">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type
                            specimen book. <br><br>
                            It has survived not only five centuries, but also the leap into
                            electronic typesetting, remaining essentially unchanged. <br><br>
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem 
                            Ipsum passages, and more recently with desktop publishing software like Aldus
                            PageMaker including versions of Lorem Ipsum.
                        </div>
                        <div class="col-md-2 col-xs-2"></div>
                    </div>
                </div>
                <div class="item col-md-6 col-sm-6 col-xs-12">
                    <img src="../../assets/frontend/onepage/img/giraffe.png" alt="NAME" class="img-responsive">
                </div>
            </div>
        </div>

        <!-- BEGIN FOOTER -->
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="copyright">Impressum AGB Datenscutz FAQ Swissquality</div>
                    </div>
                    <!--                    <div class="col-md-6 col-sm-12 pull-right">
                                            <ul class="social-icons">
                                                <li><a class="rss" data-original-title="rss" href="javascript:void(0);"></a></li>
                                                <li><a class="facebook" data-original-title="facebook" href="javascript:void(0);"></a></li>
                                                <li><a class="twitter" data-original-title="twitter" href="javascript:void(0);"></a></li>
                                                <li><a class="googleplus" data-original-title="googleplus" href="javascript:void(0);"></a></li>
                                                <li><a class="linkedin" data-original-title="linkedin" href="javascript:void(0);"></a></li>
                                                <li><a class="youtube" data-original-title="youtube" href="javascript:void(0);"></a></li>
                                                <li><a class="vimeo" data-original-title="vimeo" href="javascript:void(0);"></a></li>
                                                <li><a class="skype" data-original-title="skype" href="javascript:void(0);"></a></li>
                                            </ul>
                                        </div>-->
                </div>
            </div>
        </div>
        <!-- END FOOTER -->
        <!-- <a href="#promo-block" class="go2top scroll"><i class="fa fa-arrow-down"></i></a> -->
        <!--[if lt IE 9]>
        <script src="../../assets/global/plugins/respond.min.js"></script>
        <![endif]-->
        <!-- Load JavaScripts at the bottom, because it will reduce page load time -->
        <!-- Core plugins BEGIN (For ALL pages) -->
        <script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Core plugins END (For ALL pages) -->
        <!-- BEGIN RevolutionSlider -->
        <script src="../../assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
        <script src="../../assets/frontend/onepage/scripts/revo-ini.js" type="text/javascript"></script> 
        <!-- END RevolutionSlider -->
        <!-- Core plugins BEGIN (required only for current page) -->
        <script src="../../assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
        <script src="../../assets/global/plugins/jquery.easing.js"></script>
        <script src="../../assets/global/plugins/jquery.parallax.js"></script>
        <script src="../../assets/global/plugins/jquery.scrollTo.min.js"></script>
        <script src="../../assets/frontend/onepage/scripts/jquery.nav.js"></script>
        <!-- Core plugins END (required only for current page) -->
        <!-- Global js BEGIN -->
        <script src="../../assets/frontend/onepage/scripts/layout.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                Layout.init();
                window.scrollTo(0, 0);
            });
        </script>
        <!-- Global js END -->
    </body>
</html>